package pdm.unindubria.ripetizionapp20;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.firestore.FirebaseFirestore;

public class StudentActivity extends AppCompatActivity {
    static String materia,prezzo,luogo,fasciaOraria;
    protected EditText editTextMateria;
    protected EditText editTextLuogo;
    protected EditText editTextPrice;
    protected EditText editTextFasciaOraria;
    private Button SendSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {// inizializzazione elementi classe student
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);
        Toolbar myToolbar=findViewById(R.id.myToolbar);
        setSupportActionBar(myToolbar);
        editTextLuogo = findViewById(R.id.editTextTextPersonName);
        editTextMateria = findViewById(R.id.editTextTextPersonName3);
        editTextPrice = findViewById(R.id.editTextNumberDecimal);
        editTextFasciaOraria=findViewById(R.id.editTextTextPersonName4);
        SendSearch= (Button) findViewById(R.id.buttonStudentSendSearch);
        SendSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { //listener quando si clicca sul bottone si apre l'activity con i risulati della ricerca
                materia=editTextMateria.getText().toString();
                materia=materia.trim();
                prezzo=editTextPrice.getText().toString();
                prezzo=prezzo.trim();
                luogo=editTextLuogo.getText().toString();
                luogo=luogo.trim();
                fasciaOraria=editTextFasciaOraria.getText().toString();
                fasciaOraria=fasciaOraria.trim();
                TeacherActivity.permesso=true;
                openListSearch();

            }
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //metodo per il menu toolBar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }
    public void openListSearch(){ // menu per inviare l'intento che apre l'activity contenete i risultati della ricerca

        Intent intentStudentSendSearch = new Intent(this,ListTeacherSearchActivity.class);
        startActivity(intentStudentSendSearch);
    }
}