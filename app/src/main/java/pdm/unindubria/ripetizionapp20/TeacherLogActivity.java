package pdm.unindubria.ripetizionapp20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class TeacherLogActivity extends AppCompatActivity {

    EditText textViewPass;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<Professore> profList;
    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {// inizzializzazione elementi activity e firebase
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_log);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview_productsLog);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        profList = new ArrayList<>();

        mAdapter=new PersonAdapter(this,profList);
        recyclerView.setAdapter(mAdapter);


        db = FirebaseFirestore.getInstance();


        db.collection("Professore").whereEqualTo("email",TeacherActivity.email).get() // query per restituire i dati relativi all'email inserita
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {




                        if(!queryDocumentSnapshots.isEmpty()){

                            List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();

                            for(DocumentSnapshot d : list){

                                Professore p = d.toObject(Professore.class);
                                profList.add(p);

                            }

                            mAdapter.notifyDataSetChanged();

                        }


                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(TeacherLogActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    
}