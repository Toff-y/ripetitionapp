package pdm.unindubria.ripetizionapp20;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class AdminLogActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private ArrayList<Professore> profList;
    private FirebaseFirestore db;
    @Override
    protected void onCreate(Bundle savedInstanceState) { //inizializzazione elementi dell'activity e il firebase
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_log);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerview_productsAdmin);
        recyclerView.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        profList = new ArrayList<>();

        mAdapter=new PersonAdapter(this,profList);
        recyclerView.setAdapter(mAdapter);


        db = FirebaseFirestore.getInstance();


        db.collection("Professore").get() //query che mostra tutti i prof esistenti nel firestore
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {




                        if(!queryDocumentSnapshots.isEmpty()){

                            List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();

                            for(DocumentSnapshot d : list){

                                Professore p = d.toObject(Professore.class);
                                profList.add(p);

                            }

                            mAdapter.notifyDataSetChanged();

                        }


                    }
                });
    }
}