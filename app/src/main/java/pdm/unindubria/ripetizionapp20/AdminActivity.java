package pdm.unindubria.ripetizionapp20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class AdminActivity extends AppCompatActivity {
    protected static boolean Stato=false;
    protected EditText emailEditText;
    protected EditText passwordEditText;
    protected Button logInButton;
    String emailLT = "luca.2399@libero.it";
    String emailLM = "luca.marra@gmail.com";
    String pwLT = "123456";
    String pwLM = "123456789";
    protected TextView signUpTextView;
    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {// inizializzazione degli elementi dell'activity e firebase
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        mFirebaseAuth = FirebaseAuth.getInstance();
        emailEditText = findViewById(R.id.emailEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        logInButton = findViewById(R.id.loginButton);

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // listener login controllo elementi inseriti, se corrispondono a quella dei due amministratori
                boolean permesso=true;
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                email = email.trim();
                password = password.trim();
                if (email.isEmpty() || password.isEmpty()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this);
                    builder.setMessage(R.string.login_error_message)
                            .setTitle(R.string.login_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    if (email.equals(emailLT)) {
                        if (password.equals(pwLT)) {
                            TeacherActivity.permesso=true;
                            permesso=false;
                            Intent intent = new Intent(AdminActivity.this,
                                    AdminLogActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            Stato=true;
                            TeacherActivity.permesso=true;
                            startActivity(intent);
                        }
                    } else if (email.equals(emailLM)) {
                        if (password.equals(pwLM)) {
                            permesso=false;
                            TeacherActivity.permesso=true;
                            Intent intent = new Intent(AdminActivity.this,
                                    AdminLogActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            Stato=true;

                            startActivity(intent);

                        }
                    } else if (permesso){
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdminActivity.this);
                        builder.setMessage(R.string.login_error_message)
                                .setTitle(R.string.login_error_title)
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                    permesso=true;

                }

            }
        });
    }
}