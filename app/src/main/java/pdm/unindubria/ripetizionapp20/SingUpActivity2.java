package pdm.unindubria.ripetizionapp20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SingUpActivity2 extends AppCompatActivity {
        protected EditText passwordEditText;
        protected EditText fullName;
        protected EditText emailEditText;
        protected Button signUpButton;
        protected EditText prezzoEditText;
        protected EditText materiadEditText;
        protected EditText luogodEditText;
        protected EditText fasciaOrariadEditText;
        private FirebaseAuth mFirebaseAuth;
        FirebaseFirestore fstore;
        Random rnd=new Random();
        int id=rnd.nextInt(100);
        String userID;
        TextView textViewData;

        @Override
        protected void onCreate(Bundle savedInstanceState) { //inzializzazione elementi activity, firebase firestore, Authentication firebase
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_sing_up2);
                fstore = FirebaseFirestore.getInstance();
                mFirebaseAuth = FirebaseAuth.getInstance();
                fullName = (EditText) findViewById(R.id.nameEditText);
                fasciaOrariadEditText=(EditText) findViewById(R.id.editTextTextFasciaOraria);
                prezzoEditText = (EditText) findViewById(R.id.editTextNumberPrezzo);
                materiadEditText = (EditText) findViewById(R.id.editTextMateria);
                luogodEditText = (EditText) findViewById(R.id.editTextTextLuogo);
                emailEditText = (EditText) findViewById(R.id.emailEditText);
                passwordEditText = (EditText) findViewById(R.id.passwordEditText);

                signUpButton = (Button) findViewById(R.id.registerButton); // listener del bottone singup
                signUpButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                int Id=rnd.nextInt(100);
                                final String id=Integer.toString(Id);
                                final String password = passwordEditText.getText().toString().trim();
                                final String email = emailEditText.getText().toString().trim();
                                final String FullName = fullName.getText().toString().trim();
                                final String prezzo = prezzoEditText.getText().toString().trim();
                                final String materia = materiadEditText.getText().toString().trim();
                                final String luogo = luogodEditText.getText().toString().trim();
                                final String Commento="bene";
                                final String fasciaOaria= fasciaOrariadEditText.getText().toString().trim();
                                if (TextUtils.isEmpty(email)) {
                                        emailEditText.setError("Email è richiesta");
                                        return;
                                }
                                if (TextUtils.isEmpty(password)) {
                                        passwordEditText.setError("Password è richiesta");
                                        return;
                                }
                                if (password.length() < 6) {
                                        passwordEditText.setError("Password deve avere almeno 6 caratteri");
                                }

                                mFirebaseAuth.createUserWithEmailAndPassword(email, password) //fase di creazione di un nuovo utente e inserimento dati nel firebase firestore
                                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                @Override
                                                public void onComplete(@NonNull Task<AuthResult> task) {
                                                        if (task.isSuccessful()) {
                                                                Toast.makeText(SingUpActivity2.this, "User created", Toast.LENGTH_SHORT).show();
                                                                userID = mFirebaseAuth.getCurrentUser().getUid();
                                                                DocumentReference dbProf = fstore.collection("Professore").document(id);


                                                                Map<String,Object> prof =new HashMap<>();
                                                                prof.put("name",FullName);
                                                                prof.put("materia",materia);
                                                                prof.put("luogo",luogo);
                                                                prof.put("email",email);
                                                                prof.put("password",password);
                                                                prof.put("prezzo",prezzo);
                                                                prof.put("commento",Commento);
                                                                prof.put("id",id);
                                                                prof.put("fasciaOraria",fasciaOaria);
                                                                dbProf.set(prof)
                                                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                                @Override
                                                                                public void onSuccess(Void aVoid) {
                                                                                        Toast.makeText(SingUpActivity2.this, "Note saved", Toast.LENGTH_SHORT).show();
                                                                                }
                                                                        })
                                                                        .addOnFailureListener(new OnFailureListener() {
                                                                                @Override
                                                                                public void onFailure(@NonNull Exception e) {
                                                                                        Toast.makeText(SingUpActivity2.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                                                                }
                                                                        });


                                                                startActivity(new Intent(getApplicationContext(), TeacherActivity.class));
                                                        } else {
                                                                Toast.makeText(SingUpActivity2.this, "Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                                        }
                                                }
                                                        });
                                                }
                                        });
                        }
                }






