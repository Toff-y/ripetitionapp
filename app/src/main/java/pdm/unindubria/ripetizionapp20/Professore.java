package pdm.unindubria.ripetizionapp20;

public  class Professore {
    private String name, materia, luogo, email,password,prezzo,id;

    private String commento,fasciaOraria;


    public Professore(){

    }



    public Professore(String name, String materia, String Luogo, String Email, String Password, String Prezzo,String Commento,String Id,String FasciaOraria) {//cotruttore classe professore
        this.name = name;
        this.materia = materia;
        this.luogo = Luogo;
        this.prezzo = Prezzo;
        this.email= Email;
        this.password=Password;
        this.commento=Commento;
        this.id=Id;
        this.fasciaOraria=FasciaOraria;
    }
    // metodi get per ogni campo della classe
    public String getEmail() {
        return email;
    }


    public String getName() {
        return name;
    }

    public String getMateria() {
        return materia;
    }

    public String getLuogo() {
        return luogo;
    }

    public String getPrezzo() {
        return prezzo;
    }

    public String getId() {
        return id;
    }

    public String getFasciaOraria() {
        return fasciaOraria;
    }

    public String getCommento() {
        return commento;
    }
}
