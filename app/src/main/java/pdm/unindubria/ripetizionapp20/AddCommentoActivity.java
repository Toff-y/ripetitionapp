package pdm.unindubria.ripetizionapp20;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;

public class AddCommentoActivity extends AppCompatActivity implements View.OnClickListener {
private Button Add;
private Button Delete;
private EditText editTextCommento;


    FirebaseFirestore fstore;
    DocumentReference dbProf;
    @Override
    protected void onCreate(Bundle savedInstanceState) { // metodo per l'inizzializzazione elemnti dell'actity e il firebase
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_commento);
        editTextCommento=findViewById(R.id.editTextCommentoAdd);

        Add= findViewById(R.id.buttonADD);
        Add.setOnClickListener(this);
        Delete=findViewById(R.id.buttonDelete);
        Delete.setOnClickListener(this);
        fstore = FirebaseFirestore.getInstance();



}

    public void onClick(View view) { //metodo per la gestione del listener dei bottoni dell'activity
        if (AdminActivity.Stato) {
            switch (view.getId()) {
                case R.id.buttonADD:
                    AddCommento();
                    break;
                case R.id.buttonDelete:
                    deleteCommento();
                    break;

                default:
                    break;

            }
        }else {
            AddCommento();
        }
    } //metodi per l'aggiunta di un commento o cancellazione
    public void AddCommento(){
        dbProf = fstore.collection("Professore").document(PersonAdapter.id);
        String description = editTextCommento.getText().toString();
        dbProf.update("commento",description);

    }
    public void deleteCommento(){
        dbProf = fstore.collection("Professore").document(PersonAdapter.id);
        dbProf.update("commento", FieldValue.delete());
    }
}