package pdm.unindubria.ripetizionapp20;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class PersonAdapter extends RecyclerView.Adapter<PersonAdapter.ProductViewHolder> {

    private Context mCtx;
    private ArrayList<Professore> professoreListList;
    static String id;
    public PersonAdapter(Context mCtx, ArrayList<Professore> profList) {
        this.mCtx = mCtx;
        this.professoreListList = profList;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProductViewHolder(
                LayoutInflater.from(mCtx).inflate(R.layout.layoutprof, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull  final ProductViewHolder holder, int position) { // essegnamento valori ad ogni elemento della view
        Professore product = professoreListList.get(position);
        holder.textViewID.setText(product.getId());
        holder.textViewName.setText(product.getName());
        holder.textViewEmail.setText(product.getEmail());
        holder.textViewLuogo.setText(product.getLuogo());
        holder.textViewMateria.setText(product.getMateria());
        holder.textViewPrezzo.setText(product.getPrezzo());
        holder.textViewCommento.setText(product.getCommento());
        holder.textViewFasciaOraria.setText(product.getFasciaOraria());
        holder.textViewID.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("ResourceType")
            @Override
            public void onClick(View view) { // listener che quando si clicca sull'id si può modificare il commento
                 id=holder.textViewID.getText().toString();

                id=id.trim();
                if (TeacherActivity.permesso){

                Intent intent = new Intent(mCtx,AddCommentoActivity.class);
                mCtx.startActivity(intent);

            }

        }
    });
    }

    @Override
    public int getItemCount() {
        return professoreListList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder  { // creazione textView per l'adapter per il recycler view

        TextView textViewName, textViewEmail, textViewLuogo, textViewMateria, textViewPrezzo,textViewCommento, textViewID, textViewFasciaOraria;

        public ProductViewHolder(View itemView) {
            super(itemView);
            textViewID=itemView.findViewById(R.id.editTextIDLayout);
            textViewCommento= itemView.findViewById(R.id.textViewCommentoLayout);
            textViewName = itemView.findViewById(R.id.textview_name);
            textViewEmail = itemView.findViewById(R.id.textview_email);
            textViewLuogo = itemView.findViewById(R.id.textview_Luogo);
            textViewPrezzo = itemView.findViewById(R.id.textview_price);
            textViewMateria = itemView.findViewById(R.id.textview_Materia);
            textViewFasciaOraria=itemView.findViewById(R.id.textview_fasciaOraria);

        }

    }

}