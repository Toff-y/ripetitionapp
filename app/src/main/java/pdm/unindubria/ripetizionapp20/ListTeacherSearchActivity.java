package pdm.unindubria.ripetizionapp20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import java.util.ArrayList;
import java.util.List;

public class ListTeacherSearchActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;


    private ArrayList<Professore> profList;
    private FirebaseFirestore db;


        @Override
        protected void onCreate(Bundle savedInstanceState) {// inizializzazione elementi dell'activity e firebase
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_list_teacher_search);

            recyclerView = (RecyclerView)findViewById(R.id.recyclerview_products);
            recyclerView.setHasFixedSize(true);
            layoutManager=new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);

            profList = new ArrayList<>();

            mAdapter=new PersonAdapter(this,profList);
            recyclerView.setAdapter(mAdapter);


            db = FirebaseFirestore.getInstance(); // query in base agli elemnti inserity nell'activity studente e popolazione della recycler view


            db.collection("Professore").whereEqualTo("materia",StudentActivity.materia).whereEqualTo("luogo",StudentActivity.luogo).whereEqualTo("fasciaOraria",StudentActivity.fasciaOraria).whereEqualTo("prezzo",StudentActivity.prezzo).get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            db.collection("Professore").getId();
                            if(!queryDocumentSnapshots.isEmpty()){

                                List<DocumentSnapshot> list = queryDocumentSnapshots.getDocuments();

                                for(DocumentSnapshot d : list){

                                    Professore p = d.toObject(Professore.class);
                                    profList.add(p);

                                }

                                mAdapter.notifyDataSetChanged();

                            }


                        }
                    });

        }
}