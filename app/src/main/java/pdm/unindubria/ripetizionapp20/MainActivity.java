package pdm.unindubria.ripetizionapp20;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener{

    private Button studentButton;
    private Button TeacherButton;
    private Button AdminButton;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private String mUserId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {// inizializzazione elementi classe main
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        studentButton= (Button) findViewById(R.id.buttonStudent);
        studentButton.setOnClickListener(this);
        TeacherButton=(Button) findViewById(R.id.buttonInseg);
        TeacherButton.setOnClickListener(this);
        AdminButton=(Button) findViewById(R.id.buttonAmmi);
        AdminButton.setOnClickListener(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        Toolbar myToolbar=findViewById(R.id.myToolbar);
        setSupportActionBar(myToolbar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //metodo per creare il menu ToolBar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.example_menu, menu);
        return true;
    }
    public void onClick(View view){ // metodo click quando si cliccano i bottoni
        switch (view.getId()) {
            case R.id.buttonStudent:
                openStudentActivity();
                break;
            case R.id.buttonInseg:
                openTeachActivity();
                break;
            case R.id.buttonAmmi:
                openAdminActivity();
                break;
            default:
                break;

        }
    }
// metodi per aprire l'activity in base al bottone che si clicca con degli intenti
    public void openStudentActivity(){
        Intent intentStudent = new Intent(this,StudentActivity.class);
        startActivity(intentStudent);
    }
    public void openTeachActivity(){
        Intent intentTeacher = new Intent(this,TeacherActivity.class);
        startActivity(intentTeacher);
    }
    public void openAdminActivity(){
        Intent intentAdmin = new Intent(this,AdminActivity.class);
        startActivity(intentAdmin);
    }
    private void loadLogInView() {
        Intent intent = new Intent(this, TeacherActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}